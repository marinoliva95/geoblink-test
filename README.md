# geoblink-fe-vue

This is the Vue.js template for Geoblink's Frontend position exercise.

## Project structure

You can safely **ignore** these folders:

- `backend`
- `build`
- `config`

The only files you should modify are in `src` folder.

## Build Setup

``` bash
# install dependencies
npm i

# serve with hot reload at localhost:5001 (backend at localhost:5002)
npm start
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## About the backend

Any request to the backend will return the list of states after a delay of 1 second.
Note that they might fail randomly.
